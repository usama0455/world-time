import 'package:flutter/material.dart';

class AuthenticationWidgets {
  BoxDecoration backgroundImage() {
    return BoxDecoration(
      image: DecorationImage(
        image: AssetImage('assets/background.png'),
        fit: BoxFit.cover,
      ),
    );
  }

  Widget buildTextFieldTitle(String title) {
    return Text(
      title,
      style: TextStyle(
        fontFamily: 'SFUID-Medium',
        fontSize: 18.0,
        color: Color.fromARGB(255, 28, 37, 44).withOpacity(0.8),
      ),
      textAlign: TextAlign.left,
    );
  }

  Widget buildPageTitle(BuildContext context, String title) {
    return SizedBox(
        width: 200.0,
        height: 40.0,
        child: FittedBox(
          child: Text(
            //MyLocalizations.of(context,'loginPage', title),
            title,
            style: TextStyle(
                fontFamily: 'SFUID-Medium',
                fontSize: 32.0,
                color: Color.fromARGB(255, 28, 37, 44)),
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
          ),
        ));
  }

  Widget logo() {
    return Container(
      child:
          //App Logo
          Image.asset('assets/logo.png'),
    );
  }

  Widget buildBottomRow(BuildContext context, String text, String buttonText) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        SizedBox(
          width: MediaQuery.of(context).size.width * 70 / 100,
          height: 20.0,
          child: FittedBox(
            child: Row(
              children: <Widget>[
                Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 18.0,
                    fontFamily: 'SFUID-Medium',
                    color: Color.fromARGB(255, 28, 37, 44).withOpacity(0.6),
                    letterSpacing: 1.0,
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
                InkWell(
                  child: Text(
                    buttonText,
                    style: TextStyle(
                      fontSize: 18.0,
                      fontFamily: 'SFUID-Medium',
                      color: Color.fromARGB(255, 28, 37, 44).withOpacity(0.7),
                      fontWeight: FontWeight.w500,
                    ),
                    textAlign: TextAlign.right,
                  ),
                  onTap: () {
                    buttonText == 'Login'
                        ? Navigator.pushReplacementNamed(context, '/login')
                        : Navigator.pushReplacementNamed(context, '/signup');
                  },
                ),
              ],
            ),
          ),
        ),

//
      ],
    );
  }
}
