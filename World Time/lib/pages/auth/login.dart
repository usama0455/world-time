import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'autentcation-widgets.dart';
import '../home.dart';
import 'package:world_time/services/world_time.dart';

class loginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
// TODO: implement createState
    return _loginPageState();
  }
}

class _loginPageState extends State<loginPage> {
  final GlobalKey<FormState> _loginForm = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool isSavedCredentialsChecked = false;

  FocusNode _emailFocus = FocusNode();
  FocusNode _passwordFocus = FocusNode();


  String _email;
  String _password;
  bool _inProgress = false;
  Map data = {};

  Widget _buildEmailTextField() {
    return TextFormField(
      focusNode: _emailFocus,
      controller: _emailController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        fillColor: Color(0xFFf7f6f6),
        filled: true,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        hintText: 'muhammad.usama455@gmail.com',
        hintStyle:
            TextStyle(color: Colors.grey[500], fontSize: 18.0, height: 1.0),
        contentPadding: EdgeInsets.only(top: 2.0, right: 10.0),
        prefixIcon: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5.0),
          child: Icon(
            Icons.email,
            color: Colors.grey[600],
            size: 18.0,
          ),
        ),
      ),
      style: TextStyle(height: 0.8, color: Colors.black, fontSize: 18.0),
      validator: (String value) {
        if (value.isEmpty ||
            !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                .hasMatch(value)) {
          return 'Enter valid email address';
        }
        return null;
      },
      onSaved: (String value) {
        _email = value;
      },
    );
  }

  Widget _buildPasswordTextField() {
    return TextFormField(
        controller: _passwordController,
        decoration: InputDecoration(
          fillColor: Color(0xFFf7f6f6),
          filled: true,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          hintText: '********',
          hintStyle:
              TextStyle(color: Colors.grey[500], fontSize: 18.0, height: 1.6),
          contentPadding: EdgeInsets.only(top: 2.0, right: 10.0),
          prefixIcon: Padding(
            padding: EdgeInsets.symmetric(horizontal: 5.0),
            child: Icon(
              Icons.lock,
              color: Colors.grey[600],
              size: 18.0,
            ),
          ),
        ),
        style: TextStyle(height: 0.8, color: Colors.black, fontSize: 18.0),
        obscureText: true,
        validator: (String value) {
          if (value.isEmpty || value.length < 6) {
            return 'Password must be atleast 6 characters long';
          }
          return null;
        },
        onSaved: (String value) {
          _password = value;
        });
  }
    Widget _buildForgetPasswordInkWell() {
    return InkWell(
        child: Text(
          'Forget Password?',
          style: TextStyle(
              fontSize: 16.0,
              fontFamily: 'SFUID-Medium',
              color: Color.fromARGB(255, 28, 37, 44).withOpacity(0.4),
              height: 2.0),
          textAlign: TextAlign.right,
        ),
        onTap: () {
          setState(() {
            Navigator.pushReplacementNamed(context, '/forget-password');
          });
        });
  }


//  void _loginOnTap() async {
//     if (!_loginForm.currentState.validate()) {
//       return;
//     }
//     _loginForm.currentState.save();
//     if (_email == null || _password.length < 5) {
//       return;
//     }
//     setState(() {
//       _inProgress = true;
//     });

//     _email = _email.toLowerCase();

//     MainModel _model = new MainModel();

//     _model.login(_email, _password).then((Map<String, dynamic> response) {
//       print(response);
//       if (response['success']) {
//         //login success
//         _model.getGreenGrassIP().then((onValue) {
//           if (onValue == 'success') {
//             //ggipc success
//             env.isLoggedIn = true;
//             Navigator.pushReplacementNamed(context, '/dashboard/0');
//           } else {
//             setState(() {
//               _inProgress = false;
//               _model.logout();
//               _showSnackBar('Your apartment is not ready!!!');
//             });
//           }
//         });
//       } else {
//         setState(() {
//           _inProgress = false;
//         });
//         _showSnackBar(response['message']);
//       }
//     });
//   }

  @override
  Widget build(BuildContext context) {
  
  double keyboardStatus = 0.0;
    setState(() {
      keyboardStatus = MediaQuery.of(context).viewInsets.bottom;
    });
return Scaffold(
      resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Container(
          decoration: AuthenticationWidgets().backgroundImage(),
          padding:
              EdgeInsets.only(left: 15.0, right: 15.0, top: 45.0, bottom: 20.0),
          child: Material(
            color: Colors.white.withOpacity(0.8),
//            shape: Border(
//                top: BorderSide(color: Colors.lightBlueAccent, width: 7.0)),
            child: Container(
              child: Column(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        colors: [
                          // Color.fromARGB(255, 33, 0, 100),
                          // Color.fromARGB(255, 256, 25, 100),
                          Color(0xFF09f3af),
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(0.9, 0.0),
                      ),
                    ),
                    height: 10.0,
                    //margin: EdgeInsets.only(bottom: 0.0),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(
                          top: 0.0, left: 30.0, right: 30.0, bottom: 30.0),
                      child: Form(
                          key: _loginForm,
                          child: ListView(
                              padding: EdgeInsets.only(top: 0.0),
                              children: <Widget>[
                                Container(
                                  child:
                                      //App Logo
                                      Image.asset('assets/logo.png'),
                                ),
                                AuthenticationWidgets()
                                    .buildPageTitle(context, 'Login'),
                                SizedBox(
                                  height: 25.0,
                                ),
                                AuthenticationWidgets()
                                    .buildTextFieldTitle('  Email'),
                                SizedBox(
                                  height: 5.0,
                                ),
                                _buildEmailTextField(),
                                SizedBox(
                                  height: 20.0,
                                ),
                                AuthenticationWidgets()
                                    .buildTextFieldTitle('  Password'),
                                SizedBox(
                                  height: 5.0,
                                ),
                                _buildPasswordTextField(),
                                SizedBox(
                                  height: 5.0,
                                ),
                                _buildForgetPasswordInkWell(),
                                SizedBox(
                                  height: 10.0,
                                ),
                                // _inProgress == true
                                    // ? new Theme(
                                    //     data: Theme.of(context)
                                    //         .copyWith(accentColor: Colors.blue),
                                    //     child: Center(
                                    //         child:
                                    //             new CircularProgressIndicator()),
                                    //   )
                                    // : 
                                    Container(
                                        width: (300.0),
                                        height:
                                            MediaQuery.of(context).size.height *
                                                7 /
                                                100,
                                        decoration: BoxDecoration(
                                          gradient: LinearGradient(
                                            colors: [
                                              Color.fromARGB(255, 46, 134, 193),
                                              Color.fromARGB(255, 0, 153, 175),
                                              // Color(0xFF09f3af),
                                            ],
                                            begin: const FractionalOffset(
                                                0.0, 0.0),
                                            end: const FractionalOffset(
                                                0.9, 0.0),
                                          ),
                                          borderRadius:
                                              BorderRadius.circular(30.0),
                                        ),
                                          child: FlatButton(
                                              padding: EdgeInsets.only(
                                                  left: 25.0, right: 25.0),
                                              child: Text(
                                                'Login',
                                                style: TextStyle(
                                                    fontFamily: 'SFUID-Medium',
                                                    fontSize: 18.0,
                                                    color: Colors.white),
                                              ),
                                              onPressed: () async{

                                                //  Navigator.push(context,MaterialPageRoute(builder: (context) => Home()),
                                                //   );
                                                  WorldTime wt = WorldTime(location: 'Karchi', flag: 'pakistan.png', url: 'Asia/Karachi');
                                                     await wt.getTime();

                                                    Navigator.pushReplacementNamed(context, '/loading');
                                                    // Navigator.pushReplacementNamed(context, '/home', arguments: {
                                                    //   'location': wt.location,
                                                    //   'flag': wt.flag,
                                                    //   'time': wt.time,
                                                    //   'isDaytime': wt.isDaytime,
                                                    // });
                                                },
                                            //   highlightColor: Colors
                                            //       .lightBlueAccent
                                            //       .withOpacity(0.5),
                                            //   splashColor: Colors
                                            //       .lightGreenAccent
                                            //       .withOpacity(0.5),
                                            ),                                        
                                    ),
                                    
                                          // child:
                                              // ScopedModelDescendant<MainModel>(
                                                  // builder:
                                                      // (BuildContext context,
                                //                           Widget child,
                                //                           MainModel model) {
                                //             return MaterialButton(
                                //               padding: EdgeInsets.only(
                                //                   left: 25.0, right: 25.0),
                                //               child: Text(
                                //                 'Login',
                                //                 style: TextStyle(
                                //                     fontFamily: 'SFUID-Medium',
                                //                     fontSize: 18.0,
                                //                     color: Colors.white),
                                //               ),
                                //               onPressed: () {
                                //                 // _loginOnTap(model);
                                //               },
                                //               highlightColor: Colors
                                //                   .lightBlueAccent
                                //                   .withOpacity(0.5),
                                //               splashColor: Colors
                                //                   .lightGreenAccent
                                //                   .withOpacity(0.5),
                                //             );
                                //           }),
                                //           color: Colors.transparent,
                                //           borderRadius:
                                //               BorderRadius.circular(30.0),
                                //         ),
                                //       ),
                                SizedBox(
                                  height: 20.0,
                                ),
                                AuthenticationWidgets().buildBottomRow(context,
                                    'Don\'t have an account?', 'Signup'),
                                SizedBox(
                                  height: 5.0,
                                ),
                                Container(
                                  height: keyboardStatus == 0.0
                                      ? 0.0
                                      : MediaQuery.of(context)
                                          .viewInsets
                                          .bottom,
                                ),
                              ])

                          //_buildChild(),
                          ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}